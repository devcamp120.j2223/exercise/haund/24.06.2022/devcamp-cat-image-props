import { Component } from "react";
import Animal from "./components/animal";
class App extends Component {
  render() {
    return (
      <div>
        <Animal kind = "cat" />
      </div>
    );
  }
}

export default App;
