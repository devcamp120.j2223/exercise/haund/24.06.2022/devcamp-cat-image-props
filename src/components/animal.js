import { Component } from "react";
import cat from "../assets/images/cat.jpg"

class Animal extends Component {
    render() {
        const { kind } = this.props;
        if (kind == "cat") {
            return (
                <div>
                    <p>Display result of kind (kind is "cat"): </p>
                    <img src={cat} alt="cat-img" />
                </div>
            )
        } else {
            return (
                <div>
                    <p>Display result of kind (kind not equal "cat"): </p>
                     <p><b>Meow is not found :)</b></p>
                </div>
            )
        }
    }
}

export default Animal;